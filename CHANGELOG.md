This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Bom

## [v2.4.1]

- Enhanced common-authorization lower bound of range
- Enhanced authorization-client lower bound of range
- Enhanced keycloak-client lower bound of range
- Enhanced gxHTTP lower bound of range
- Enhanced gxJRS lower bound of range
- Enhanced storagehub-model range
- Enhanced storagehub-client-library range
- Added document-store-lib-accounting-service


## [v2.4.0]

- Enhanced information-system-model version range
- Enhanced gcube-model version version range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Enhanced resource-registry-context-client lower bound of range
- Enhanced resource-registry-query-template-client lower bound of range
- Enhanced resource-registry-schema-client lower bound of range
- Added keycloak-client


## [v2.3.0] - 2023-03-29

- upgrade lower bound range on gCube common dependencies. See #24727


## [v2.2.0]

- Enhanced information-system-model version range
- Enhanced gcube-model version lower bound of range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Added common-utility-sg3


## [v2.1.0]

- Enhanced gcube-model version range
- Enhanced information-system-model version range
- Added json-simple library version 1.1.1 [#21692]
- Added storagehub dependencies in range 2,3 [#22730] [#22872]
- Removed authorization-utils
- Enhanced logback-classic version to 1.2.4


## [v2.0.2]

- Added storagehub dependecies in range 1,2 [#22925]  
- Added authorization-utils [#22550][#22871]


## [v2.0.1] [r5.0.0] - 2021-02-24 

- Updated resource registry clients versions

## [v2.0.0] [r4.26.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19283]


## [v1.5.0] [r4.21.0] - 2020-03-30

- Removed -SNAPSHOT from dependencies lower bound of ranges


## [v1.4.0] [r4.17.0] - 2019-12-19

- Removed -SNAPSHOT from dependencies lower bound of ranges


## [v1.3.0] [r4.14.0] - 2019-05-27

- Added gxREST Dependency
- Added gxJRS Dependency
- Imported jersey-bom
- Added javax.ws.rs-api compatible with defined jersey version


## [v1.2.1] [r4.13.1] - 2019-02-26

- Upgraded slf4j-api from 1.7.5 to 1.7.25
- Added gxHTTP Dependency


## [v1.2.0] [r4.13.0] - 2018-11-20

- Updated resource-registry related clients and libraries [#11941]


## [v1.1.0] [r4.8.0] - 2017-11-29

- Added Jackson libraries [#9835]


## [v1.0.2] [r4.5.0] - 2017-06-06

- Changed version of accounting-lib and document-store-lib


## [v1.0.1] [r4.2.0] - 2016-12-15

- Removed wrong scope on dependency declaration


## [v1.0.0] [r4.1.0] - 2016-11-07

- First Release

