<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.gcube.tools</groupId>
		<artifactId>maven-parent</artifactId>
		<version>1.2.0</version>
	</parent>
	<groupId>org.gcube.distribution</groupId>
	<artifactId>gcube-bom</artifactId>
	<version>2.4.1</version>
	<packaging>pom</packaging>
	<name>gCube BOM</name>
	<description>gCube Bom contains commons gCube library versions to avoid version conflicts among gCube components</description>

	<properties>
		<gcube-jackson.version>2.8.11</gcube-jackson.version>
		<jersey.version>2.25.1</jersey.version>
		<lombok.version>1.18.4</lombok.version>
		<javax.version>2.3.1</javax.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<scm>
		<connection>scm:git:https://code-repo.d4science.org/gCubeSystem/gcube-bom-legacy.git</connection>
		<developerConnection>scm:git:https://code-repo.d4science.org/gCubeSystem/gcube-bom-legacy.git</developerConnection>
		<url>https://code-repo.d4science.org/gCubeSystem/gcube-bom-legacy</url>
	</scm>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId> org.glassfish.jersey </groupId>
				<artifactId>jersey-bom</artifactId>
				<version>${jersey.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<!-- Jersey 2.25.1 is compliant with javax.ws.rs-api 2.0.1 Higher version 
				of Jersey uses higher version of javax.ws.rs-api. Please check javax.ws.rs-api 
				requirements before updating Jersey version -->
			<dependency>
				<groupId>javax.ws.rs</groupId>
				<artifactId>javax.ws.rs-api</artifactId>
				<version>2.0.1</version>
			</dependency>
			
			<!-- Added to support Java 11 JDK -->
			<dependency>
				<groupId>org.projectlombok</groupId>
				<artifactId>lombok</artifactId>
				<version>${lombok.version}</version>
			</dependency>
			<dependency>
   				<groupId>javax.xml.bind</groupId>
   				<artifactId>jaxb-api</artifactId>
   				<version>${javax.version}</version>
			</dependency>
			<dependency>
    			<groupId>javax.xml.ws</groupId>
    			<artifactId>jaxws-api</artifactId>
    			<version>${javax.version}</version>
			</dependency>
			<dependency>
				<groupId>com.sun.xml.bind</groupId>
				<artifactId>jaxb-impl</artifactId>
				<version>${javax.version}</version>
			</dependency>
			<dependency>
			    <groupId>com.sun.xml.bind</groupId>
			    <artifactId>jaxb-core</artifactId>
			    <version>2.3.0.1</version>
			</dependency>
			<dependency>
			    <groupId>javax.activation</groupId>
			    <artifactId>activation</artifactId>
			    <version>1.1.1</version>
			</dependency>
			<!-- END Added to support Java 11 JDK -->

			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-encryption</artifactId>
				<version>[1.2.0,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.resources</groupId>
				<artifactId>common-gcore-resources</artifactId>
				<version>[1.3.5,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-gcore-stubs</artifactId>
				<version>[1.2.4,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-scope</artifactId>
				<version>[1.3.1,2.0.0-SNAPSHOT)</version> 
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-scope-maps</artifactId>
				<version>[1.1.1,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.resources</groupId>
				<artifactId>registry-publisher</artifactId>
				<version>[1.3.2,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.resources.discovery</groupId>
				<artifactId>ic-client</artifactId>
				<version>[1.0.5,2.0.0-SNAPSHOT)</version> 
			</dependency>
			<dependency>
				<groupId>org.gcube.resources.discovery</groupId>
				<artifactId>discovery-client</artifactId>
				<version>[1.0.3,2.0.0-SNAPSHOT)</version>
			</dependency>

			<!-- Storagehub dependencies -->
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>storagehub-model</artifactId>
				<version>[2.0.0, 3.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>storagehub-client-library</artifactId>
				<version>[2.0.0, 3.0.0-SNAPSHOT)</version>
			</dependency>
			<!-- END Storagehub dependencies -->

			<!-- Facets Based Resource Model related dependencies -->
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>information-system-model</artifactId>
				<version>[7.0.0,8.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.resource-management</groupId>
				<artifactId>gcube-model</artifactId>
				<version>[5.0.0,6.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>resource-registry-api</artifactId>
				<version>[5.0.0,6.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>resource-registry-client</artifactId>
				<version>[4.4.0,5.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>resource-registry-publisher</artifactId>
				<version>[4.4.0,5.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>resource-registry-context-client</artifactId>
				<version>[4.1.1,5.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>resource-registry-query-template-client</artifactId>
				<version>[1.1.1,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.information-system</groupId>
				<artifactId>resource-registry-schema-client</artifactId>
				<version>[4.2.1,5.0.0-SNAPSHOT)</version>
			</dependency>
			<!-- END Facets Based Resource Model related dependencies -->

			<!-- Clients Related Dependencies -->
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-generic-clients</artifactId>
				<version>[1.0.1,2.0.0-SNAPSHOT)</version> 
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-gcube-calls</artifactId>
				<version>[1.2.0,2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-jaxws-calls</artifactId>
				<version>[1.0.2,2.0.0-SNAPSHOT)</version> 
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-clients</artifactId>
				<version>[2.1.1,3.0.0-SNAPSHOT)</version> 
			</dependency>
			<dependency>
				<groupId>org.gcube.core</groupId>
				<artifactId>common-fw-clients</artifactId>
				<version>[1.1.1,2.0.0-SNAPSHOT)</version> 
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>gxREST</artifactId>
				<version>[1.0.0, 2.0.0-SNAPSHOT)</version> <!-- maybe this component can be removed-->
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>gxHTTP</artifactId>
				<version>[1.2.0, 2.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>gxJRS</artifactId>
				<version>[1.2.0, 2.0.0-SNAPSHOT)</version>
			</dependency>
			<!-- END Clients Related Dependencies -->

			<!-- Authorization Related Dependencies -->
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>authorization-client</artifactId>
				<version>[2.0.7,3.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>common-authorization</artifactId>
				<version>[2.5.1,3.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>keycloak-client</artifactId>
				<version>[2.1.0,3.0.0-SNAPSHOT)</version>
			</dependency>
			<!-- END Authorization Related Dependencies -->

			<!-- Accounting Related Dependencies -->
			<dependency>
				<groupId>org.gcube.data.publishing</groupId>
				<artifactId>document-store-lib</artifactId>
				<version>[3.0.0,4.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.data.publishing</groupId>
				<artifactId>document-store-lib-accounting-service</artifactId>
				<version>[2.0.0,3.0.0-SNAPSHOT)</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.accounting</groupId>
				<artifactId>accounting-lib</artifactId>
				<version>[4.0.0,5.0.0-SNAPSHOT)</version>
			</dependency>
			<!-- END Accounting Related Dependencies -->


			<!-- Used for compatibility between SG 3 and 4 -->
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>common-utility-sg3</artifactId>
				<version>[1.0.0,2.0.0-SNAPSHOT)</version>
			</dependency>
			<!-- END Used for compatibility between SG 3 and 4 -->

			<dependency>
				<groupId>ch.qos.logback</groupId>
				<artifactId>logback-classic</artifactId>
				<version>1.2.4</version>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>1.7.25</version>
			</dependency>

			<!-- Added for authorization -->
			<dependency>
				<groupId>com.googlecode.json-simple</groupId>
				<artifactId>json-simple</artifactId>
				<version>1.1.1</version>
			</dependency>



			<!-- gCube Jackson -->
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>gcube-jackson-databind</artifactId>
				<version>${gcube-jackson.version}</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>gcube-jackson-annotations</artifactId>
				<version>${gcube-jackson.version}</version>
			</dependency>
			<dependency>
				<groupId>org.gcube.common</groupId>
				<artifactId>gcube-jackson-core</artifactId>
				<version>${gcube-jackson.version}</version>
			</dependency>
			<!-- END gCube Jackson -->

		</dependencies>
	</dependencyManagement>

</project>
